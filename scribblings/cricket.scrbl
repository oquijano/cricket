#lang scribble/manual

@require[scribble/eval
	@for-label[cricket
		   racket/place
                   racket/base]]

@title{cricket}
@author{Oscar Alberto Quijano Xacur}

@;@defmodule[cricket]		

A package with common functions for web back-end programming.

@section["Passwords"]

@defmodule[cricket/passwords]

@defproc[(encode-password [password (or/c string? bytes?)]
			  [#:p p (or/c string? bytes?) (processor-count)]
			  [#:t t integer? 5]
			  [#:m m integer? 7168])
			  string?]{
Hashes @racket[password] using the @hyperlink["https://en.wikipedia.org/wiki/Argon2"]{argon2id} algorithm.

@racket[p] is the degree of parallelism.

@racket[t] is the minimum number of iterations

@racket[m] is the minimum memory size.

The default values of the three parameters are based on
recommendations from @hyperlink["https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html"]{OWASP}.
}
			  	
@defproc[(verify-password [input (or/c string? bytes?)]
			  [encoded-password string?])
			  boolean?]{
Returns @racket[#t] if @racket[input] is the hashed string in @racket[encoded-password], otherwise it returns @racket[#f].
}

@(define helper-eval (make-base-eval))
@interaction-eval[#:eval helper-eval
                   (require cricket/passwords)]

@examples[
#:eval helper-eval
(define hashed-password (encode-password "a-password"))
hashed-password
(verify-password "another-password" hashed-password)
(verify-password "a-password" hashed-password)
]
